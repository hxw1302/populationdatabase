﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PopulationDatabaseGroup6JH
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// //Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
    public partial class MainWindow : Window
    {
        CityVM cvm = new CityVM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = cvm;
        }
        private void SortByName_Click(object sender, RoutedEventArgs e)
        {
            cvm.DefinedSort();
        }
        private void AvgPop_Click(object sender, RoutedEventArgs e)
        {
            cvm.GetAvgPopu();
        }
        private void LowestPop_Click(object sender, RoutedEventArgs e)
        {
            cvm.GetLowestPopu();
        }
        private void new_Click(object sender, RoutedEventArgs e)
        {
            cvm.CityToEdit = new City();
            CityPopulation cpw = new CityPopulation(cvm)
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            cpw.ShowDialog();
        }
        private void update_Click(object sender, RoutedEventArgs e)
        {
            if (cvm.SelectedCity != null)
            {
                cvm.CityToEdit = cvm.SelectedCity.CityClone();
                CityPopulation cpw = new CityPopulation(cvm);
                cpw.ShowDialog();
            }
        }
        private void quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
