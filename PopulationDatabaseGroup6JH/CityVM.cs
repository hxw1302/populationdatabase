﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
//Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
namespace PopulationDatabaseGroup6JH
{
    public class CityVM : INotifyPropertyChanged
    {
        Database db = Database.GetInstance();
        BindingList<City> showCity;
        public BindingList<City> ShowCity
        {
            get { return showCity; }
            set { showCity = value; NotifyChanged(); }
        }
        City selectedCity;
        //switch to manual later
        public City SelectedCity
        {
            get { return selectedCity; }
            set { selectedCity = value; NotifyChanged(); }
        }
        public CityVM()
        {
            showCity = new BindingList<City>(db.GetCity());
        }
        City cityToEdit;
        public City CityToEdit
        {
            get { return cityToEdit; }
            set { cityToEdit = value; NotifyChanged(); }
        }
        public void Save()
        {            
            if (cityToEdit.RowID != 0)
            {
                int index = ShowCity.IndexOf(selectedCity);
                ShowCity.Remove(selectedCity); 
                ShowCity.Insert(index, cityToEdit);
            }
            else
                ShowCity.Add(cityToEdit);
            db.Save(ShowCity.ToList());
        }

        public void DefinedSort()
        {
            if (AscSort)
            {
                if (SortbyCity)                
                    db.Toshow.Sort(new SortNameAsc());                
                else if (SortbyID)                
                    db.Toshow.Sort(new SortIDAsc());                
                else if (SortbyPopu)                
                    db.Toshow.Sort(new SortCityAsc()); 
                else
                    db.Toshow.Sort(new SortCityAsc());
            }
            else if (DescSort)
            {
                if (SortbyCity)
                    db.Toshow.Sort(new SortNameDesc());
                else if (SortbyID)
                    db.Toshow.Sort(new SortIDDesc());
                else if (SortbyPopu)
                    db.Toshow.Sort(new SortCityDesc());
                else
                    db.Toshow.Sort(new SortNameDesc());
            }
            else
                db.Toshow.Sort(new SortCityAsc());    
            ShowCity = new BindingList<City>(db.Toshow);            
        }

        public class SortCityDesc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;                
                if (cone.Population < ctwo.Population)
                    return 1;
                if (cone.Population > ctwo.Population)
                    return -1;
                else
                    return 0;
            }
        }
        public class SortCityAsc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;
                if (cone.Population > ctwo.Population)
                    return 1;
                if (cone.Population < ctwo.Population)
                    return -1;
                else
                    return 0;
            }
        }
        public class SortIDDesc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;
                if (cone.RowID < ctwo.RowID)
                    return 1;
                if (cone.RowID > ctwo.RowID)
                    return -1;
                else
                    return 0;
            }
        }
        public class SortIDAsc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;
                if (cone.RowID > ctwo.RowID)
                    return 1;
                if (cone.RowID < ctwo.RowID)
                    return -1;
                else
                    return 0;
            }
        }
        public class SortNameDesc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;
                return string.Compare(y.CityName, x.CityName);
            }
        }
        public class SortNameAsc : IComparer<City>
        {
            public int Compare(City x, City y)
            {
                City cone = (City)x;
                City ctwo = (City)y;
                return string.Compare(x.CityName, y.CityName);
            }
        }
        public bool ascSort;
        public bool AscSort
        { get { return ascSort; }
          set { ascSort = value; NotifyChanged(); }
        }
        private bool descSort;
        public bool DescSort
        {
            get { return descSort; }
            set { descSort = value; NotifyChanged(); }
        }
        private bool sortbyID;
        public bool SortbyID
        {
            get { return sortbyID; }
            set { sortbyID = value; NotifyChanged(); }
        }
        private bool sortbyPopu;
        public bool SortbyPopu
        {
            get { return sortbyPopu; }
            set { sortbyPopu = value; NotifyChanged(); }
        }
        private bool sortbyCity;
        public bool SortbyCity
        {
            get { return sortbyCity; }
            set { sortbyCity = value; NotifyChanged(); }
        }
        private double avgPopu;
        public double AvgPopu
        {
            get { return avgPopu; }
            set { avgPopu = value; NotifyChanged(); }
        }
        public void GetAvgPopu()
        {                         
            AvgPopu = db.GetAverage();
        }
        private double lowestPopu;
        public double LowestPopu
        {
            get { return lowestPopu; }
            set { lowestPopu = value; NotifyChanged(); }
        }
        public void GetLowestPopu()
        {
            LowestPopu = db.GetLowest();
        }     
        #region
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
