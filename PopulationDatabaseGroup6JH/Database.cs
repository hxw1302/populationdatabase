﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
namespace PopulationDatabaseGroup6JH
{
    public class Database
    {
        const string CONNECT_MASTER = @"Server=.\; Database=master; Trusted_Connection=True;";
        const string CREATE_DATABASE = "IF DB_ID (N'Population') IS NULL CREATE DATABASE Population";
        const string CREATE_TABLE = "USE Population;" + "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'City')"+
             " CREATE TABLE City(City NVARCHAR(50) PRIMARY KEY NOT NULL, Population FLOAT NOT NULL, RowID INT NOT NULL);";
        const string CONNECT_STRING = @"Server=.\; Database=Population; Trusted_Connection=True;";
        SqlConnection connection;
        static Database database;        
        private Database()
        {                       
            connection = new SqlConnection(CONNECT_MASTER);
            //connection.Open();
            var cmd = new SqlCommand(CREATE_DATABASE, connection);
            var cmdtwo = new SqlCommand(CREATE_TABLE, connection);
            connection.Open();
            cmd.ExecuteNonQuery();
            cmdtwo.ExecuteNonQuery();
            InsertData();
        }
        public void InsertData()
        {
            string[] lines = File.ReadAllLines("city.csv");
            lines = lines.Skip(1).ToArray();            
            foreach (var line in lines)
            {  //trim all double quotes from string            
                string[] fields = line.Split(',');
                fields[0] = fields[0].Replace("\"", "");
                fields[1] = fields[1].Replace("\"", "");
                fields[2] = fields[2].Replace("\"", "");
                var cmdString = "INSERT INTO City " +
                        "(City, Population, RowID)" +
                        "VALUES" +
                        "(@City, @Population, @RowID)";
                var cmd = new SqlCommand(cmdString, connection);
                cmd.Parameters.AddWithValue("@City", fields[0]);
                cmd.Parameters.AddWithValue("@Population",  SqlDbType.Float).Value=Convert.ToDouble(fields[1]);
                cmd.Parameters.AddWithValue("@RowID", SqlDbType.Int).Value = fields[2];
                cmd.ExecuteNonQuery();
            }
        }
        public static Database GetInstance()
        {
            if (database == null)
                database = new Database();
            return database;
        }
        public double GetLowest()
        {
            
            var cmdString = "SELECT MIN(Population)" + " FROM City";
            var cmd = new SqlCommand(cmdString, connection);
            var Lowest = (double)cmd.ExecuteScalar();
            return Lowest;
        }
        public double GetAverage()
        {           
            var cmdString = "SELECT AVG(Population) As AVGS" + " FROM City";
            var cmd = new SqlCommand(cmdString, connection);
            var Average = (double)cmd.ExecuteScalar();
            return Average;
        }
        List<City> toshow = new List<City>();
        public List<City> Toshow
        {
            get { return toshow; }
            set { toshow = value;  }
        }
        public List<City> GetCity()
        {
            var ps = new List<City>();
            var cmdString = "SELECT RowID, City, Population" + " FROM City";
            var cmd = new SqlCommand(cmdString, connection);
            SqlDataReader originalshow = cmd.ExecuteReader();
            while (originalshow.Read())
                ps.Add(new City()
                {
                    RowID = originalshow.GetInt32(originalshow.GetOrdinal("RowID")),
                    CityName = originalshow.GetString(originalshow.GetOrdinal("City")),
                    Population =originalshow.GetDouble( originalshow.GetOrdinal("Population"))
                });
            originalshow.Close();
            // need to finishthis.
            Toshow = ps;
            return ps;
        }
        public int GetNextID()
        {
            var id=0;
            var cmdString = "SELECT MAX(RowID) FROM City";
            var cmd = new SqlCommand(cmdString, connection);
            var sqlresult = cmd.ExecuteScalar();
            id = (int)sqlresult;
            return id + 1;
        }
        public void Save(List<City> cp)
        {
            var citiesToDelete = new List<City>();

            foreach (City c in cp)
            {
                if (c.IsDeleted)
                {
                    Delete(c);
                    citiesToDelete.Add(c);
                }
                else if (c.RowID == 0)
                    Add(c);
                else
                    Update(c);
            }
            foreach (City c in citiesToDelete)
                cp.Remove(c);
        }
        public void Add(City c)
        {
            if (c.RowID == 0)
                c.RowID = GetNextID();

            var cmdString = "INSERT INTO City " +
                                    "(City, Population, RowID)" +
                                    "VALUES" +
                                    "(@CityName, @Population, @RowID)";

            var cmd = new SqlCommand(cmdString, connection);
            cmd.Parameters.AddWithValue("@CityName", c.CityName);
            cmd.Parameters.AddWithValue("@Population", c.Population);
            cmd.Parameters.AddWithValue("@RowID", c.RowID);
            cmd.ExecuteNonQuery();
        }
        public void Delete(City c)
        {
            var cmdString = "DELETE City " +
                               "WHERE RowID = @RowID";

            var cmd = new SqlCommand(cmdString, connection);
            cmd.Parameters.AddWithValue("@RowID", c.RowID);
            cmd.ExecuteNonQuery();
        }
        public void Update(City c)
        {
            if (c.IsChanged)
            {
                var cmdString = "UPDATE City SET City= @CityName, " +
                                                     "Population = @Population " +                                                     
                                   "WHERE RowID = @RowID";
                var cmd = new SqlCommand(cmdString, connection);
                cmd.Parameters.AddWithValue("@CityName", c.CityName);
                cmd.Parameters.AddWithValue("@Population", c.Population);
                cmd.Parameters.AddWithValue("@RowID", c.RowID);
                cmd.ExecuteNonQuery();
                c.IsChanged = false;
            }
        }








































    }
}
