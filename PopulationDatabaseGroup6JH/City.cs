﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PopulationDatabaseGroup6JH
{//Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
    public class City
    {
        public double Population { get; set; }
        public string CityName { get; set; }
        public int RowID { get; set; }
        public bool IsChanged { get; set; }
        public bool IsDeleted { get; set; }
        public City CityClone()
        {
            var c = new City
            {
                Population = Population,
                CityName = CityName,
                RowID = RowID,
                IsChanged = true,
                IsDeleted = false
            };
            return c;
        }
    }    
}
