﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
namespace PopulationDatabaseGroup6JH
{
    /// <summary>
    /// Interaction logic for CityPopulation.xaml
    /// </summary>
    public partial class CityPopulation : Window
    {
        CityVM cvm;
        public CityPopulation(CityVM cvm)
        {
            InitializeComponent();
            DataContext = cvm;
            this.cvm = cvm;
        }
        void Save_Click(object sender, EventArgs e)
        {
            //tell the vm class to save this and then close
            cvm.Save();
            this.Close();
        }
        void Cancel_Click(object sender, EventArgs e)
        {
            //just close on cancel
            this.Close();
        }
    }
}
