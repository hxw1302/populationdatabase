# Population Database Information Retriever

This is just C# based simple user inferface using MVVM to create and connect to city population database, and it allows you to retrieve basic data information such as average, highest or lowest population. It also allows
users to sort the data for differnt objects based on ascending or descending order.

## Getting Started

1. Simply download the project files or clone the repo
2. Ensure the city.csv file is in debug folder.
3. Open VisualStudio, attach csv file to the project
4. Run .sln solution file in Debug or Release mode to execute the program


### Prerequisites

1. VisualStudio 2017
2. Ensure city.csv data file attached in the project


### Installing

1. If you install the solution by downloading zipped source code, unzip the file to a direclty has no more than 266 characters
2. if you choose to clone the git, you can simply use git clone to sychronize files into the diretories you like


## Deployment

This simple solution doesn't need and doesnt have ability to be deployed in a live system;


## Authors

* **Xueweiwei (Joe) Huang** - *Initial work* - [WeiHuang](https://bitbucket.org/hxw1302)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Xuewei (Joe) Huang 


## Notes
The reason whay I choose MIT license is that MIT license permissive license which is short and simple. It has little restriciton on use and therefore excellent software
license compatibility. It only require preserving copyright and license notices. it is convenient for distribution. It has has little legal restriction and someone can takes it the project and customize
for commerical use. 
It is good for distribution. And I welcome the community to help me improve the project and improve the codes in my learning curve.
This project doesnt have any benefit conflict, I dont use to for commericial purpose and it is really simple and basic stuff for skill testing. 


